import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		try
		{
			Phone p1 = new Phone("Sony","Experia X",32,12.5,4.6,true,150);
			Phone p2 = new Phone("Sony","Experia Z",64,14.2,5.6,true,175);
			Phone p3 = new Phone("Samsung","Galaxy M",64,14.5,4.6,true,180);
			Phone p4 = new Phone("Nokia","3330",16,13.2,2.3,false,90);
			Phone p5 = new Phone("Motorola","M1",8,11.3,4.9,true,100);
			Phone p6 = new Phone("iPhone","6",32,13.5,6.4,true,250);
			Phone p7 = new Phone("Alcatel","A3",8,9.3,2.4,false,50);
			Phone p8 = new Phone("iPhone","8",32,16,9,true,400);
			
			
			List<Phone> phones = new ArrayList<Phone>(7);
			phones.add(p1);
			phones.add(p2);
			phones.add(p3);
			phones.add(p4);
			phones.add(p5);
			phones.add(p6);
			phones.add(p7);
			
			System.out.println(phones.toString());
			

			
			File outFile =  new File("phone.ser");	
			try {
				FileOutputStream fs = new FileOutputStream(outFile);
				ObjectOutputStream os = new ObjectOutputStream(fs);
				os.writeObject(phones); 
				os.close();
				
				System.out.println("\nSerialization Success\n");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			File ReadFile =  new File("phone.ser");	
			try {
				FileInputStream fis = new FileInputStream(ReadFile);
				ObjectInputStream ois = new ObjectInputStream(fis);
				  ArrayList<Phone> phoneList = (ArrayList<Phone>) ois.readObject();

				System.out.println("Deserialized File : \n"+ phoneList);
	
				
				ois.close();
				
				
				System.out.println("\nDeserialization Success\n");
				
				phoneList.remove(phoneList.get(6));
				phoneList.add(p8);
				
				System.out.println(phoneList.toString());
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		

	}

}
