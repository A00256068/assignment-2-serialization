import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;

public class Phone implements Serializable {
	
	private String make;
	private String model;
	private int memory;
	private double camera;
	private double screenSize;
	private boolean smart;
	private int price;
	private List<Phone> phones;
	
	public Phone(String make, String model,int memory,double camera,double screenSize,boolean smart,int price) throws RemoteException
	{
		this.make = make;
		this.model = model;
		this.memory = memory;
		this.camera = camera;
		this.screenSize = screenSize;
		this.smart = smart;
		this.price = price;
	}
	
	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getMemory() {
		return memory;
	}
	public void setMemory(int memory) {
		this.memory = memory;
	}
	public double getCamera() {
		return camera;
	}
	public void setCamera(double camera) {
		this.camera = camera;
	}
	public double getScreenSize() {
		return screenSize;
	}
	public void setScreenSize(double screenSize) {
		this.screenSize = screenSize;
	}
	public boolean isSmart() {
		return smart;
	}
	public void setSmart(boolean smart) {
		this.smart = smart;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	public String toString() 
    { 
        return "| Make: "+ make + "| Model: " + model + "| Memory(GB): " + memory + "| Camera(MP): " + camera + "| ScreenSize(inches): " + screenSize + "| Smart: " +smart+ "| Price(�): "+price+" |\n"; 
    }


	public List<Phone> getPhones() {
		return phones;
	}


	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}


	

}
