import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class PhoneGui {

	private static JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {					
					PhoneGui window = new PhoneGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public PhoneGui() throws RemoteException {
		initialize();

	}

	
	
	private void initialize() throws RemoteException {
		frame = new JFrame();
		 frame.setLayout(null);
		JButton button = new JButton("Show Phones");
		 button.setBounds(175,300,150,60);
		 button.setVisible(true);
		 frame.add(button);

		 JButton button1 = new JButton("Hide Phones");
		 button1.setBounds(500,300,150,60);
		 button1.setVisible(true);
		 frame.add(button1);
		 
		frame.setBounds(100, 100,800, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 JTextArea textArea = new JTextArea();
		 textArea.setBounds(50, 50, 700, 250);
		 frame.getContentPane().add(textArea);
		 
		Phone p1 = new Phone("Sony","Experia X",32,12.5,4.6,true,150);
		Phone p2 = new Phone("Sony","Experia Z",64,14.2,5.6,true,175);
		Phone p3 = new Phone("Samsung","Galaxy M",64,14.5,4.6,true,180);
		Phone p4 = new Phone("Nokia","3330",16,13.2,2.3,false,90);
		Phone p5 = new Phone("Motorola","M1",8,11.3,4.9,true,100);
		Phone p6 = new Phone("iPhone","6",32,13.5,6.4,true,250);
		Phone p7 = new Phone("Alcatel","A3",8,9.3,2.4,false,50);
		Phone p8 = new Phone("iPhone","8",32,16,9,true,400);
		
		List<Phone> phones = new ArrayList<Phone>(7);
		phones.add(p1);
		phones.add(p2);
		phones.add(p3);
		phones.add(p4);
		phones.add(p5);
		phones.add(p6);
		phones.add(p7); 
       
		    	

        button.addActionListener(new ActionListener() {

	    public void actionPerformed(ActionEvent arg0) {
		       
		 for(Phone a : phones){
			 textArea.append(a + "\n");
		   }
	     }
	});
        
        button1.addActionListener(new ActionListener() {

    	    public void actionPerformed(ActionEvent arg0) {
    		       
    		  textArea.setText(null);
    		  
    	     }
    	});
		    	
	
	}

}
